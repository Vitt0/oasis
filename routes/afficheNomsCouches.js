var express = require('express');
var router = express.Router();
const { Client } = require('pg');

router.post('/', function(req, res, next) {
  console.log(req.body);

  const client = new Client({
    connectionString: process.env.SCALINGO_POSTGRESQL_URL,
    ssl: {
      rejectUnauthorized: false
    }
  });

  client.connect(function(err) {
    if (err) {
      console.error('connection error', err.stack);
      return res.status(500).json({ error: 'Database connection error' });
    }
    console.log("Connected!");

    const { id_thematique } = req.body;
    const requete = `SELECT nom_couche, id_couche FROM public."InfoCouches" WHERE id_thematique = $1`;

    client.query(requete, [id_thematique], (err, result) => {
      if (err) {
        console.error('query error', err.stack);
        return res.status(500).json({ error: 'Query error' });
      } else {
        res.json(result.rows);
      }
      client.end();
    });
  });
});

module.exports = router;
