var express = require('express');
var router = express.Router();
const { Client } = require('pg');

// Utiliser l'URL de connexion PostgreSQL fournie par Scalingo
const connectionString = process.env.SCALINGO_POSTGRESQL_URL;

/* GET users listing. */
router.post('/', function(req, res,) {
  
  // Connexion à la base
  const client = new Client({
    connectionString: connectionString,
    ssl: {
      rejectUnauthorized: false
    }
  });

  client.connect(function(err) {
    if (err) {
      console.error('connection error', err.stack);
      res.status(500).send('Database connection error');
      return;
    }
    console.log("Connected!");

    //Requête
    console.log('body', req.body);
    const { id_couche }  = req.body;

    const requete = `
      SELECT public."InfoCouches".data, public."Styles".fill_color, public."Styles".fill_stroke, public."Styles".stroke_color, public."Styles".style_complexe, public."Styles".attribut
      FROM public."Styles"
      JOIN public."InfoCouches" ON public."Styles".id_simple = public."InfoCouches".id_style_simple
      WHERE public."InfoCouches".id_couche = $1
    `;

    client.query(requete, [id_couche], (err, result) => {
      if (err) {
        console.error('query error', err.stack);
        res.status(500).send('Query error');
      } else {
        console.log(result);
        res.json(result);
      }
      client.end();
    });
  });
});

module.exports = router;
