var express = require('express');
var router = express.Router();
const { Client } = require('pg');

router.post('/', function(req, res, next) {
  console.log(req.body);

  const client = new Client({
    connectionString: process.env.SCALINGO_POSTGRESQL_URL,
    ssl: {
      rejectUnauthorized: false
    }
  });

  client.connect(function(err) {
    if (err) {
      console.error('connection error', err.stack);
      return res.status(500).json({ error: 'Database connection error' });
    }
    console.log("Connected!");

    const { id_couche } = req.body;
    const requete = `
      SELECT public."Thematiques".nom_thematique, public."InfoCouches".nom_couche, public."InfoCouches".id_thematique, public."InfoCouches".cout, public."InfoCouches".date, public."InfoCouches".description_generale, public."InfoCouches".revisite, public."InfoCouches".lien_applisat, public."InfoCouches".obtention_couche, public."InfoCouches".resolution_spatio, public."InfoCouches".applications, public."InfoCouches".limites, public."InfoCouches".biblio, public."InfoCouches".satellite, public."InfoCouches".bandes, public."InfoCouches".id_couche
      FROM public."InfoCouches" 
      JOIN public."Thematiques" ON public."InfoCouches".id_thematique = public."Thematiques".id_themathique
      WHERE public."InfoCouches".id_couche = $1
    `;

    client.query(requete, [id_couche], (err, result) => {
      if (err) {
        console.error('query error', err.stack);
        return res.status(500).json({ error: 'Query error' });
      } else {
        res.json(result.rows);
      }
      client.end();
    });
  });
});

module.exports = router;
